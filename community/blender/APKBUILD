# Contributor: Mark Riedesel <mark@klowner.com>
# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=blender
pkgver=3.0.1
_pkgver=${pkgver%.[0-9]}
pkgrel=1
pkgdesc="3D Creation/Animation/Publishing System"
url="https://www.blender.org/"
arch="x86_64" # only on x86_64, mainly because of openvdb
license="GPL-2.0-or-later"
depends="blender-shared=$pkgver-r$pkgrel"
makedepends="cmake
	alembic-dev
	blosc-dev
	boost-dev
	eigen-dev
	embree-dev
	embree-static
	ffmpeg-dev
	fftw-dev
	freetype-dev
	glew-dev
	gmp-dev
	jack-dev
	libharu-dev
	libjpeg-turbo-dev
	libpng-dev
	libsndfile-dev
	libtbb-dev
	libx11-dev
	libxi-dev
	libxrender-dev
	llvm12-dev
	lzo-dev
	openal-soft-dev
	opencolorio-dev
	openexr-dev
	openimagedenoise-dev
	openimageio-dev
	openjpeg-dev
	opensubdiv-dev
	openvdb-dev
	openvdb-nanovdb
	openxr-dev
	osl
	osl-dev
	potrace-dev
	pugixml-dev
	pulseaudio-dev
	py3-numpy-dev
	python3-dev
	samurai
	sdl2-dev
	tiff-dev
	"
subpackages="$pkgname-doc $pkgname-shared::noarch $pkgname-headless py3-$pkgname:python"
source="https://download.blender.org/source/blender-$pkgver.tar.xz
	0001-musl-fixes.patch
	0002-fix-linking-issue.patch
	0003-increase-thread-stack-size-for-musl.patch
	0004-OpenEXR-3.patch
	0005-OpenImageIO-2.3.patch
	"


build() {
	# Headless
	mkdir -p "$builddir"/build-headless
	cd "$builddir"/build-headless
	_build -C../build_files/cmake/config/blender_headless.cmake

	# Full
	mkdir -p "$builddir"/build-full
	cd "$builddir"/build-full
	_build -C../build_files/cmake/config/blender_full.cmake

	# Python module
	mkdir -p "$builddir"/build-py
	cd "$builddir"/build-py
	_build -C../build_files/cmake/config/bpy_module.cmake
}

_build() {
	local _py_version=$(python3 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))')

	export CFLAGS="$CFLAGS -D__MUSL__"
	export CXXFLAGS="$CXXFLAGS -D__MUSL__"

	cmake .. "$@" \
		-G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DWITH_PYTHON_INSTALL=OFF \
		-DWITH_INSTALL_PORTABLE=OFF \
		-DWITH_SYSTEM_LZO=ON \
		-DWITH_SYSTEM_EIGEN3=ON \
		-DWITH_SYSTEM_GLEW=ON \
		-DPYTHON_VERSION=$_py_version

	cmake --build .
}

package() {
	# Install headless files
	cd "$builddir"/build-headless
	DESTDIR="$pkgdir"/headless cmake --install .

	# Rename the headless blender to blender-headless
	mkdir -p "$pkgdir"/usr/bin
	mv "$pkgdir"/headless/usr/bin/blender "$pkgdir"/usr/bin/blender-headless
	rm -rf "$pkgdir"/headless

	# Install python module
	cd "$builddir"/build-py
	DESTDIR="$pkgdir" cmake --install .

	# Install the full package
	cd "$builddir"/build-full
	DESTDIR="$pkgdir" cmake --install .
}

shared() {
	pkgdesc="Blender shared runtime data and add-on scripts"
	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/blender "$subpkgdir"/usr/share/
}

headless() {
	pkgdesc="$pkgdesc (headless build)"
	depends="blender-shared=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/blender-headless "$subpkgdir"/usr/bin/
}

python() {
	local _py_version=$(python3 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))')

	pkgdesc="Blender modules for Python 3"
	mkdir -p "$subpkgdir"/usr/lib/python"$_py_version"/site-packages
	mv "$pkgdir"/usr/lib/python"$_py_version"/site-packages/bpy.so "$subpkgdir"/usr/lib/python"$_py_version"/site-packages/
	rm -rf "$pkgdir"/usr/lib

	# Symlink to the blender-shared files
	ln -s /usr/share/blender/"$_pkgver" "$subpkgdir"/usr/lib/python"$_py_version"/site-packages/"$_pkgver"
}

sha512sums="
b9af6d49201eb26ec77a3cecdb9c0945ffc915d4eecaaa36091365340244bfd1565a7679c8b7a81d3335383f158dc01339ea8edc108730835d81db1de84049bf  blender-3.0.1.tar.xz
f06f37cc7b8d306e85a07691d89d68685f7af5d0aeffafec2b2c2404508f27705d2b85528e23bd55d644035d328b0494586e77c3b69e3eafb183825eda438846  0001-musl-fixes.patch
9e6f7bde5e4678595a1fc7f31275a8f5987a10eeed1ab97eb361b23f9010adff9d861cc1ebb7cc8888450e3b209a60823e5c58d226430f050fb27e0248a0c3bf  0002-fix-linking-issue.patch
e29532cc121be46cc6538499cc23f7dc3df06c6e202ae746ef377f167ba9480341f9753f6db81a25998ca860ed583ae08c1e45d259f20de6b2580ad07854bd6e  0003-increase-thread-stack-size-for-musl.patch
91861d6a2ae47ff38c204bbe279f3f393b3cf3e42667ce928ec7cf55e6896a4f8a92fbe6975860f26751ffebf9305c52c2f00b286a4b98626bb8addc23f86e04  0004-OpenEXR-3.patch
81a0419306c4973c1b708b7548fa0b059a1041763fb17e8bfcf80df2a69eb6b45554bb5d5a457fdb1fa790ae9293f46e063626d054c4708d115486e7e8f31eeb  0005-OpenImageIO-2.3.patch
"
